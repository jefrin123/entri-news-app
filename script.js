let currentQuery = "weather"
let currentPage = 1

const fetchNews = async  (page, q) => {
  console.log(`fetching news for ${q} Page Number ${page}....`)
  var url = 'https://newsapi.org/v2/everything?' +
    'q=' +q+
    '&from=2024-04-27&' +
    'pageSize=20&' +
    'language=en&' +
    'page=' +page+
    '&sortBy=popularity&' +
    'apiKey=e2a3a05cec34440a890e3c3fc0d782a8';

  var req = new Request(url);

  let a = await fetch(req)
  let response = await a.json()
  console.log(response)


// console.log(JSON.stringify(response))
//  let response = {
//   "status": "ok",
//   "totalResults": 1265,
//   "articles": [
//     {
//       "source": {
//         "id": null,
//         "name": "Xataka.com"
//       },
//       "author": "Ricardo Aguilar",
//       "title": "Motorola quiere asaltar el mercado español y tiene claro cómo hacerlo: atacando justo en la nostalgia",
//       "description": "Hay una compañía con un objetivo de crecimiento poco habitual: duplicar su cuota de mercado en Europa en este 2024. No es una cifra tan disparatada si tenemos en cuenta que, en 2023, la compañía subió al top 4 de ventas y tuvo un crecimiento interanual del 73…",
//       "url": "https://www.xataka.com/empresas-y-economia/motorola-quiere-asaltar-mercado-espanol-tiene-claro-como-hacerlo-atacando-justo-nostalgia",
//       "urlToImage": "https://i.blogs.es/fa0c52/motorola/840_560.jpeg",
//       "publishedAt": "2024-04-27T15:01:32Z",
//       "content": "Hay una compañía con un objetivo de crecimiento poco habitual: duplicar su cuota de mercado en Europa en este 2024. No es una cifra tan disparatada si tenemos en cuenta que, en 2023, la compañía subi… [+6438 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Xataka.com"
//       },
//       "author": "Javier Pastor",
//       "title": "Quizás sea el momento de pasar a un navegador centrado en la privacidad: aquí están los siete más destacados",
//       "description": "Antes ni nos preocupábamos por eso de la privacidad. Usábamos navegadores y motores de búsqueda alegremente, pero cuando nos empezamos a dar cuenta de que Google era el gran hermano (o más bien, uno de ellos), algunos empezaron (empezamos) a reaccionar y a in…",
//       "url": "https://www.xataka.com/aplicaciones/quizas-sea-momento-pasar-a-navegador-centrado-privacidad-aqui-estan-siete-destacados",
//       "urlToImage": "https://i.blogs.es/6e5874/browsers1/840_560.jpeg",
//       "publishedAt": "2024-04-27T10:31:30Z",
//       "content": "Antes ni nos preocupábamos por eso de la privacidad. Usábamos navegadores y motores de búsqueda alegremente, pero cuando nos empezamos a dar cuenta de que Google era el gran hermano (o más bien, uno … [+11485 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Xataka.com"
//       },
//       "author": "Alejandro Alcolea",
//       "title": "Hemos descubierto la tumba de Platón en papiros carbonizados hace 2.000 años. Y la IA tiene parte del mérito",
//       "description": "Hace 2.000 años, Pompeya quedó congelada en el tiempo. Es lo que nos ha permitido descubrir que el genoma de un pompeyano del 79 d.C. no era muy diferente al de un italiano de la actualidad, o las camas ajustables que tenían los esclavos. La erupción del volc…",
//       "url": "https://www.xataka.com/investigacion/hemos-descubierto-tumba-platon-papiros-carbonizados-hace-2-000-anos-ia-tiene-parte-merito",
//       "urlToImage": "https://i.blogs.es/e9fe88/platon/840_560.jpeg",
//       "publishedAt": "2024-04-28T14:31:41Z",
//       "content": "Hace 2.000 años, Pompeya quedó congelada en el tiempo. Es lo que nos ha permitido descubrir que el genoma de un pompeyano del 79 d.C. no era muy diferente al de un italiano de la actualidad, o las ca… [+6463 chars]"
//     },
//     {
//       "source": {
//         "id": "fox-news",
//         "name": "Fox News"
//       },
//       "author": "Kurt Knutsson, CyberGuy Report",
//       "title": "Apple sends out threat notifications in 92 countries warning about spyware",
//       "description": "Apple recently notified a small number of iPhone customers in 92 countries that their phones may be under attack by mercenary spyware.",
//       "url": "https://www.foxnews.com/tech/apple-sends-out-threat-notifications-in-92-countries-warning-about-spyware",
//       "urlToImage": "https://static.foxnews.com/foxnews.com/content/uploads/2024/04/4-How-to-respond-to-an-Apple-threat-notification.jpg",
//       "publishedAt": "2024-04-27T10:00:26Z",
//       "content": "Recently, a small number of iPhone users in 92 countries received an unexpected notification from Apple. \r\nIt was a stark warning that their devices might be under attack by mercenary spyware known f… [+5548 chars]"
//     },
//     {
//       "source": {
//         "id": "fox-news",
//         "name": "Fox News"
//       },
//       "author": "Kurt Knutsson, CyberGuy Report",
//       "title": "Hear iPhone alerts better by changing the default notification sound",
//       "description": "The rollout of Apple's iOS 17 brought a subtle \"Rebound\" notification tone that's gotten some criticism. Kurt \"CyberGuy\" Knutsson explains a fix.",
//       "url": "https://www.foxnews.com/tech/hear-iphone-alerts-better-by-changing-default-notification-sound",
//       "urlToImage": "https://static.foxnews.com/foxnews.com/content/uploads/2023/03/3-iphone.jpg",
//       "publishedAt": "2024-04-28T10:00:01Z",
//       "content": "Ever since Apple rolled out iOS 17, theres been a bit of a buzz or, should we say, a lack thereof. \r\nThe default notification tone, \"Rebound,\" has been causing quite a stir among users.\r\nIts soft and… [+3364 chars]"
//     },
//     {
//       "source": {
//         "id": "fox-news",
//         "name": "Fox News"
//       },
//       "author": "Kurt Knutsson, CyberGuy Report",
//       "title": "Put down your phone: How selfies and videos are ruining gym etiquette and invading privacy",
//       "description": "Fitness centers and workout studios are starting to establish policies that prohibit shooting phone videos to protect people's privacy.",
//       "url": "https://www.foxnews.com/tech/put-down-phone-how-selfies-videos-are-ruining-gym-etiquette-invading-privacy",
//       "urlToImage": "https://static.foxnews.com/foxnews.com/content/uploads/2024/04/1-Put-down-your-phone-distracted-man-on-yoga-mat.jpg",
//       "publishedAt": "2024-04-27T14:00:40Z",
//       "content": "Taking selfies and videos is now as common as sending emails or text messages. While taking selfies and videos in the comfort of your own home or in your own chosen company is a given, what about pub… [+6368 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "YouTube"
//       },
//       "author": "John Gruber",
//       "title": "Joanna Stern Rigs a Drone to Drop-Test Phones From 300 Feet",
//       "description": "An Apple iPhone that flew out of an Alaska Airlines flight at 16,000 feet survived without a scratch. How is that possible? Was it the phone case? Are iPhone...",
//       "url": "https://www.youtube.com/watch?v=967CHiGqyAA",
//       "urlToImage": "https://i.ytimg.com/vi/967CHiGqyAA/maxresdefault.jpg",
//       "publishedAt": "2024-04-27T22:39:52Z",
//       "content": "Long story short, if your phone drops out of a plane, hope that it lands on grass.\n\n\n ★"
//     },
//     {
//       "source": {
//         "id": "ign",
//         "name": "IGN"
//       },
//       "author": "Noah Hunter",
//       "title": "Daily Deals: Tekken 8, Apple 2024 MacBook Air, Apple Watch Series 9",
//       "description": null,
//       "url": "https://www.ign.com/articles/daily-deals-tekken-8-apple-2024-macbook-air-apple-watch-series-9",
//       "urlToImage": "https://assets-prd.ignimgs.com/2024/04/27/daily-deals-april-27-1714240268732.png?width=1280",
//       "publishedAt": "2024-04-27T17:57:07Z",
//       "content": "The weekend is here, and there are loads of exciting deals you do not want to miss! This Saturday, new deals on some amazing video games, technology, and devices have appeared, and we've rounded up o… [+3019 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Digital Trends"
//       },
//       "author": "Alex Blake",
//       "title": "MacBook Pro OLED: Here’s everything we know so far",
//       "description": "Apple is rumored to be upgrading its MacBook Pro laptops to come with OLED displays. Wondering what that might be like? Here’s everything we know so far.",
//       "url": "https://www.digitaltrends.com/computing/apple-oled-macbook-pro-rumors/",
//       "urlToImage": "https://www.digitaltrends.com/wp-content/uploads/2024/02/MacBook-Pro-gaming-Halo.jpg?resize=1200%2C630&p=1",
//       "publishedAt": "2024-04-28T11:30:30Z",
//       "content": "Jacob Roach / Digital Trends\r\nWhile many of Apple’s laptop rivals have embraced OLED screens, Apple has stuck firmly with mini-LED in its MacBook Pro — and the results have been spectacular. As we sa… [+6289 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Digital Trends"
//       },
//       "author": "David Caballero",
//       "title": "5 great Bill Skarsgård movies you should check out",
//       "description": "Bill Skarsgård will dominate this year with three high-profile releases, but if you're seeking other films that he stars in, look no further than these gems.",
//       "url": "https://www.digitaltrends.com/movies/bill-skarsgard-movies-to-watch-april-2024/",
//       "urlToImage": "https://www.digitaltrends.com/wp-content/uploads/2024/04/Bill-Skarsgard-in-John-Wick-Chapter-4.jpg?resize=1200%2C630&p=1",
//       "publishedAt": "2024-04-27T12:30:06Z",
//       "content": "Image via Gunpowder &amp; Sky\r\nBill Skarsgård is more than just a very handsome face. In fact, he is among his generation’s most interesting and arresting performers,  a modern scream king known for … [+6103 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Digital Trends"
//       },
//       "author": "Monica J. White",
//       "title": "The real reason so many laptops have moved to soldered RAM",
//       "description": "Soldered RAM is becoming very common in laptops of all shapes and sizes -- but is that a good thing?",
//       "url": "https://www.digitaltrends.com/computing/why-laptops-in-2024-use-soldered-ram/",
//       "urlToImage": "https://www.digitaltrends.com/wp-content/uploads/2022/07/framework-laptop-upgrade.jpg?resize=1200%2C630&p=1",
//       "publishedAt": "2024-04-27T15:00:54Z",
//       "content": "Jacob Roach / Digital Trends\r\nThe completely redesigned Dell XPS 14 and 16 came out this year as two of the most divisive laptops in recent memory. No, it wasn’t just the capacitive touch buttons or … [+17154 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Disconnect.blog"
//       },
//       "author": "Paris Marx",
//       "title": "The Vision Pro is a big flop",
//       "description": "The product’s failure should further dispel the myth of tech inevitability",
//       "url": "https://disconnect.blog/the-vision-pro-is-a-big-flop/",
//       "urlToImage": "https://disconnect.blog/content/images/size/w1200/2024/04/visionpro.png",
//       "publishedAt": "2024-04-27T02:56:20Z",
//       "content": "Can you remember an Apple product unveiling that went as bad as the Vision Pro? The company known for the iPhone and the Mac had a big task: to convince people to stick a computer on their faces for … [+9631 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Gingerbeardman.com"
//       },
//       "author": null,
//       "title": "Adding the \"Move to Trash\" function to System 7.1",
//       "description": "First, a little bit of Macintosh History. You probably know that on modern macOS you can select a file in Finder, on your Desktop, or in an app, and send it ...",
//       "url": "https://blog.gingerbeardman.com/2024/04/12/adding-the-move-to-trash-function-to-system-7/",
//       "urlToImage": null,
//       "publishedAt": "2024-04-27T15:07:10Z",
//       "content": "First, a little bit of Macintosh History. You probably know that on modern macOS you can select a file in Finder, on your Desktop, or in an app, and send it to the Trash by choosing the Move to Trash… [+7418 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "Computer.rip"
//       },
//       "author": null,
//       "title": "Microsoft at Work",
//       "description": "Comments",
//       "url": "https://computer.rip/2024-04-26-microsoft-at-work.html",
//       "urlToImage": null,
//       "publishedAt": "2024-04-27T21:39:53Z",
//       "content": "I haven't written anything for a bit. I'm not apologizing, because y'all don't\r\npay me enough to apologize, but I do feel a little bad. Part of it is just that\r\nI've been busy, with work and travel a… [+14046 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "heise online"
//       },
//       "author": "Judith Hohmann",
//       "title": "Click Boom Flash: Neue Podcast-Folge \"Kinderfotos: Kette vs. Solofotografin\"",
//       "description": "David gegen Goliath in der Kindergarten- und Babyfotografie – Wie meistern Solofotografen ihre Arbeit im Schatten großer Ketten?",
//       "url": "https://www.heise.de/news/Click-Boom-Flash-Neue-Podcast-Folge-Kinderfotos-Kette-vs-Solofotografin-9698694.html",
//       "urlToImage": "https://heise.cloudimg.io/bound/1200x1200/q85.png-lossy-85.webp-lossy-85.foil1/_www-heise-de_/imgs/18/4/5/7/9/7/8/3/CtFoto_Podcast_16_9-7c539ba3ce9faccd.png",
//       "publishedAt": "2024-04-28T09:57:00Z",
//       "content": "Strohballen, dekoriert mit Plastikblumen, und dann ganz laut Spaghetti rufen einige kennen das noch aus ihrer Kindheit. Die Eltern bekamen eine Mappe mit dem Motiv in Farbe und Sepia und mussten ents… [+1463 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "9to5Mac"
//       },
//       "author": "Chance Miller",
//       "title": "Apple users are being locked out of their Apple IDs with no explanation",
//       "description": "There appears to be an increasingly widespread Apple ID outage of some sort impacting Apple users tonight. A number of people across social media say that they were logged out of their Apple ID across multiple devices on Friday evening and forced to reset the…",
//       "url": "https://9to5mac.com/2024/04/26/signed-out-of-apple-id-account-problem-password/",
//       "urlToImage": "https://i0.wp.com/9to5mac.com/wp-content/uploads/sites/6/2023/05/apple-id-down.jpg?resize=1200%2C628&quality=82&strip=all&ssl=1",
//       "publishedAt": "2024-04-27T02:59:56Z",
//       "content": "There appears to be an increasingly widespread Apple ID outage of some sort impacting users tonight. A number of people on social media say that they were logged out of their Apple ID across multiple… [+3023 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "9to5Mac"
//       },
//       "author": "Arin Waichulis",
//       "title": "Security Bite: Did Apple just declare war on Adload malware?",
//       "description": "Following the release of new betas last week, Apple snuck out one of the most significant updates to XProtect I’ve ever seen. The macOS malware detection tool added 74 new Yara detection rules, all aimed at a single threat, Adload. So what is it exactly, and …",
//       "url": "https://9to5mac.com/2024/04/28/security-bite-did-apple-just-declare-war-on-adload-malware/",
//       "urlToImage": "https://i0.wp.com/9to5mac.com/wp-content/uploads/sites/6/2024/01/Security-Bite-FI-1.png?resize=1200%2C628&quality=82&strip=all&ssl=1",
//       "publishedAt": "2024-04-28T14:02:44Z",
//       "content": "Following the release of new betas last week, Apple snuck out one of the most significant updates to XProtect I’ve ever seen. The macOS malware detection tool added 74 new Yara detection rules, all a… [+4172 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "9to5Mac"
//       },
//       "author": "Bradley Chambers",
//       "title": "Apple @ Work: Dashlane adds Splunk integration to analyze user activity data",
//       "description": "Apple @ Work is exclusively brought to you by Mosyle, the only Apple Unified Platform. Mosyle is the only solution that integrates in a single professional-grade platform all the solutions necessary to seamlessly and automatically deploy, manage & protect App…",
//       "url": "https://9to5mac.com/2024/04/27/dashlane-adds-splunk-integration-to-monitor-user-activity-data/",
//       "urlToImage": "https://i0.wp.com/9to5mac.com/wp-content/uploads/sites/6/2024/04/Dashlane-Splunk.png?resize=1200%2C628&quality=82&strip=all&ssl=1",
//       "publishedAt": "2024-04-27T12:00:00Z",
//       "content": "Apple @ Work is exclusively brought to you by Mosyle, the only Apple Unified Platform. Mosyle is the only solution that integrates in a single professional-grade platform all the solutions necessary … [+3376 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "9to5Mac"
//       },
//       "author": "Benjamin Mayo",
//       "title": "New Apple Pencil to include haptic feedback and new gestures",
//       "description": "Alongside the new iPad Pro and iPad Air lineup, Apple’s May 7 event will also feature updates to the iPad accessories, with new generation of Magic Keyboard and Apple Pencil.\n\n\n\nBloomberg’s Mark Gurman reports today that the new Pencil will include haptic fee…",
//       "url": "https://9to5mac.com/2024/04/28/new-apple-pencil-haptic-feedback/",
//       "urlToImage": "https://i0.wp.com/9to5mac.com/wp-content/uploads/sites/6/2023/10/iPad-Apple-Pencil.jpg?resize=1200%2C628&quality=82&strip=all&ssl=1",
//       "publishedAt": "2024-04-28T12:33:55Z",
//       "content": "Alongside the new iPad Pro and iPad Air lineup, Apple’s May 7 event will also feature updates to the iPad accessories, with new generation of Magic Keyboard and Apple Pencil.\r\nBloomberg’s Mark Gurman… [+1242 chars]"
//     },
//     {
//       "source": {
//         "id": null,
//         "name": "9to5Mac"
//       },
//       "author": "Chance Miller",
//       "title": "Apple’s ‘Let Loose’ iPad event said to include a special event in London as well",
//       "description": "Apple has a special event set for May 7, during which it is expected to “Let loose” and debut big changes to the iPad lineup. In an interesting twist, it sounds like there will be a companion event held by Apple in London that day as well. \n\n\n\n more…",
//       "url": "https://9to5mac.com/2024/04/27/apples-let-loose-ipad-event-said-to-include-a-special-event-in-london-as-well/",
//       "urlToImage": "https://i0.wp.com/9to5mac.com/wp-content/uploads/sites/6/2024/04/let-loose.jpg?resize=1200%2C628&quality=82&strip=all&ssl=1",
//       "publishedAt": "2024-04-27T23:05:04Z",
//       "content": "Apple has a special event set for May 7, during which it is expected to “Let loose” and debut big changes to the iPad lineup. In an interesting twist, it sounds like there will be a companion event h… [+1852 chars]"
//     }
//   ]
// }


 
let str = ''

resultCount.innerHTML = response.totalResults

for ( let item of response.articles){
str = str + `<div  class="card my-4 mx-2" style="width: 18rem;">
<img height="180" src="${item.urlToImage}" class="card-img-top" alt="...">
<div class="card-body">
    <h5 class="card-title">${item.title}</h5>
   <p class="card-text">${item.description}</p>
    <a href="${item.url}" target="_blank"  class="btn btn-primary">Read More</a>
</div>
</div>
`


}
  document.querySelector(".content").innerHTML = str

}


fetchNews(1,currentQuery) //calling function

search.addEventListener("click" ,(event) => {
  event.preventDefault()
  let query = searchInput.value
  currentQuery = query 
  fetchNews(1, query)
} )




previous.addEventListener("click" ,(event) => {
  event.preventDefault()
  console.log("previous button runinng")
  if(currentPage>1){

    currentPage = currentPage - 1
   
    fetchNews(currentPage, currentQuery)
  }
} )



next.addEventListener("click" ,(event) => {
  event.preventDefault()
  console.log("next button runinng")
  
  currentPage = currentPage + 1 
  fetchNews(currentPage, currentQuery)
  
} )